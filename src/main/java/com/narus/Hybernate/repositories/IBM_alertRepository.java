package com.narus.Hybernate.repositories;

import com.narus.Hybernate.models.IBM_Allert;
import org.springframework.data.repository.CrudRepository;

public interface IBM_alertRepository extends CrudRepository<IBM_Allert, Long> {

}