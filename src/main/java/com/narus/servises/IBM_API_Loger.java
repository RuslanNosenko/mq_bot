package com.narus.servises;

public class IBM_API_Loger {
    private static volatile IBM_API_Loger instance;

    public static IBM_API_Loger getInstance() {
        IBM_API_Loger currentInstance;
        if (instance == null) {
            synchronized (IBM_API_Loger.class) {
                if (instance == null) {
                    instance = new IBM_API_Loger();
                }
                currentInstance = instance;
            }
        } else {
            currentInstance = instance;
        }
        return currentInstance;
    }

    public String fetchMemory(int cityId, int userId, String language, String units){

        return  "ok";
    }

    public String fetchWeatherCurrent(String toString, Integer userId, String language, String unitsSystem) {
        return  "ok";
    }

    public String fetchWeatherCurrentByLocation(Float longitude, Float latitude, Integer id, String language, String unitsSystem) {
        return  "ok";
    }
}
